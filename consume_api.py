import requests
import json

api_url = 'http://api.stackexchange.com/2.2/questions?order=desc&sort=activity&site=stackoverflow'

response = requests.get(url=api_url)

for item in response.json()['items']:
    if item['answer_count'] == 0:
        print(item['title'])
        print()