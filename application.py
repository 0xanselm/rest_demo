import os
from flask import Flask, render_template, url_for
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
db = SQLAlchemy(app)
app.app_context().push()


class Drink(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    description = db.Column(db.String(120))

    def __repr__(self):
        return f"{self.name} - {self.description}"

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/drinks')
def get_drinks():
    return render_template('drinks.html', name="Hi Mom", cwd=os.getcwd(), u=url_for('get_drinks'), mylist=[1,2,3])  

@app.route('/vars')
def get_vars():
    return dict(os.environ)

@app.route('/url')
def url():
    return ('Now you are here :)')

